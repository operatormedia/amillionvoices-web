import {
  Component,
  OnInit
} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {LegislatorService} from "../service/legislator.service";
import {Legislator} from "../model/legislator";
import {CallService} from "../service/call.service";

declare const Twilio: any;

@Component({
  selector: 'legislators',
  styleUrls: [ 'legislators.component.css' ],
  templateUrl: 'legislators.component.html'
})
export class LegislatorsComponent implements OnInit {

  public sub: Params;
  public legislators: Legislator[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private legislatorService: LegislatorService,
    private callService: CallService
  ) {}

  public ngOnInit() {
    this.callService.getToken().subscribe(data => {
      console.log(data);
      Twilio.Device.setup(data.token);
    });

    this.sub = this.activatedRoute.params.subscribe(
      params => {
        this.legislatorService.list(params['latitude'], params['longitude']).subscribe(
          legislators => {
            this.legislators = legislators;
            console.log(this.legislators);
          },
          () => {
            alert('Something went wrong. Please try again.');
          }
        );
      }
    );
  }

  public call(phoneNumber: string) {
    Twilio.Device.connect({ To: '7734016613' });
  }
}
