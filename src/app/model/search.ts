export class Search {
  public loc: number[];
  public formattedAddress: string;

  public constructor(latitude: number, longitude: number, formattedAddress: string) {
    this.loc = [latitude, longitude];
    this.formattedAddress = formattedAddress;
  }
}
