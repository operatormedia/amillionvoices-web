import {
  Component,
  OnInit, NgZone, ElementRef, ViewChild
} from '@angular/core';
import {FormControl} from "@angular/forms";
import {MapsAPILoader} from "angular2-google-maps/core";
import {SearchService} from "../service/search.service";
import {Search} from "../model/search";
import {Router} from "@angular/router";

@Component({
  selector: 'home',
  styleUrls: [ './home.component.css' ],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  public searchControl: FormControl;
  public placeResult: google.maps.places.PlaceResult;

  @ViewChild("search")
  public searchElementRef: ElementRef;

  constructor(
    private mapsApiLoader: MapsAPILoader,
    private ngZone: NgZone,
    private searchService: SearchService,
    private router: Router
  ) {}

  public ngOnInit() {
    this.searchControl = new FormControl();

    this.mapsApiLoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });

      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          this.placeResult = autocomplete.getPlace();

          if (this.placeResult.geometry === undefined || this.placeResult.geometry === null) {
            return;
          }
        });
      });
    });
  }

  public findByAddress() {
    let search: Search = new Search(this.placeResult.geometry.location.lat(), this.placeResult.geometry.location.lng(),
      this.placeResult.formatted_address);

    this.searchService.create(search).subscribe(
      () => this.router.navigate(['/legislators/' + search.loc[0] + '/' + search.loc[1]])
    );
  }

  public findByLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        let search: Search = new Search(position.coords.latitude, position.coords.longitude, null);

        this.searchService.create(search).subscribe(
          () => this.router.navigate(['/legislators/' + position.coords.latitude + '/' + position.coords.longitude])
        );
      });
    }
  }
}
