import {SearchService} from "./search.service";

export * from './search.service';

export const APP_SERVICE_PROVIDERS = [
  SearchService
];

export const APP_TEST_SERVICE_PROVIDERS = APP_SERVICE_PROVIDERS;
