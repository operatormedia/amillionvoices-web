import {Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {HttpService} from "./http.service";

import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class CallService {

  constructor(private httpService:HttpService) {
  }

  getToken():Observable<any> {
    return this.httpService.get('/call/token').map(this.extractOne);
  }

  private extractOne(res: Response) {
    let body = res.json();
    console.log(body);
    return body;
  }
}
