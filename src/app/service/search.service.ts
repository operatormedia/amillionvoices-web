import {Inject, Injectable} from "@angular/core";
import {Response} from "@angular/http";
import {HttpService} from "./http.service";
import {Search} from "../model/search";

import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class SearchService {

  constructor(private httpService:HttpService) {
  }

  create(search: Search):Observable<Search> {
    return this.httpService.post('/search', JSON.stringify(search)).map(this.extractOne);
  }

  private extractOne(res: Response) {
    let body = res.json();
    return body;
  }
}
