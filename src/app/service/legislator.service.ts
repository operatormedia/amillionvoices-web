import {Inject, Injectable} from "@angular/core";
import {Response, Http} from "@angular/http";
import {HttpService} from "./http.service";
import {Search} from "../model/search";

import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {Legislator} from "../model/legislator";

@Injectable()
export class LegislatorService {

  constructor(private http: Http) {
  }

  list(latitude: number, longitude: number):Observable<Legislator[]> {
    let requestUrl = 'https://congress.api.sunlightfoundation.com/legislators/locate?latitude=' +
      latitude + '&longitude=' + longitude;

    return this.http.get(requestUrl).map(this.extractList);
  }

  private extractList(res: Response) {
    let body = res.json();
    return body.results || { };
  }
}
