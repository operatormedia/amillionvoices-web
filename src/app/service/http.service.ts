import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";
import {
  Http,
  Response, Headers, RequestOptions, RequestOptionsArgs
} from "@angular/http";

const setHeaders = (options:RequestOptionsArgs) => {
  let newOptions = new RequestOptions({}).merge(options);
  let newHeaders = new Headers(newOptions.headers);

  newHeaders.set('Accept', 'application/json');
  newHeaders.set('Content-Type', 'application/json');

  newOptions.headers = newHeaders;

  return newOptions;
};

@Injectable()
export class HttpService {

  urlPrefix:string = 'https://amillionvoices-platform.herokuapp.com/api';

  constructor(public http:Http) {
  }

  get(url:string, options?:RequestOptionsArgs):Observable<Response> {
    return this.http.get(this.urlPrefix + url, setHeaders(options));
  }

  post(url:string, body:string, options?:RequestOptionsArgs):Observable<Response> {
    return this.http.post(this.urlPrefix + url, body, setHeaders(options));
  }

  put(url:string, body:string, options?:RequestOptionsArgs):Observable<Response> {
    return this.http.put(this.urlPrefix + url, body, setHeaders(options));
  }

  delete(url:string, options?:RequestOptionsArgs):Observable<Response> {
    return this.http.delete(this.urlPrefix + url, setHeaders(options));
  }

  patch(url:string, body:string, options?:RequestOptionsArgs):Observable<Response> {
    return this.http.patch(this.urlPrefix + url, body, setHeaders(options));
  }

  head(url:string, options?:RequestOptionsArgs):Observable<Response> {
    return this.http.head(this.urlPrefix + url, setHeaders(options));
  }
}
